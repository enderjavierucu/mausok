import 'package:flutter/material.dart';
import 'package:triftshop_app/CartPage.dart';

class HomeAppBar extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(25),
      child: Row(
        children: [
          
         
          
          Badge(
            
            padding: EdgeInsets.all(7),
            
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
              },
              child: Icon (
                Icons.shopping_bag_outlined,
              )
            )
          ),
            Spacer(),
           Padding(
            padding: EdgeInsets.only(
              left: 20,
            ),
            child: Text(
              "Mausok Vape Shop",
              style: TextStyle(fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Colors.blue),
            ),
          ),
        ],
      )
    );
  }
}