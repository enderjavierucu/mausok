import 'package:flutter/material.dart';
import 'package:triftshop_app/Widgets/ItemPage.dart';

class ItemsWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return GridView.count(
      
      childAspectRatio: 0.50,
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 3,
      shrinkWrap: true,
      children: [
        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {
               Navigator.push(context, MaterialPageRoute(builder: (context)=>ItemPage()));
              },
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/1.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Toha Disposable",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱499",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v8.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Flava Disposable",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱499",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),
        
Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v9.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Enjoy Disposable",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱499",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/2.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "OXVA Xslim Pro",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱1,199",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/3.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Geekvape Legend V1",
                style: TextStyle(fontSize: 10, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱1,499",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/4.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Empire Mini",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱1,599",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),






        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v4.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Geekvape Boost 2 Pro",
                style: TextStyle(fontSize: 10, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱1,899",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v5.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Geekvape Touch",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱2,449",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v6.png",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Geekvape Legend V2",
                style: TextStyle(fontSize: 10, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱1,899",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/5.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Chill Bill eJuice",
                style: TextStyle(fontSize: 12, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱199",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v10.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Frutta kool eJuice",
                style: TextStyle(fontSize: 11, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱99",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

        Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10),
          margin: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                
                  Icon(
                    Icons.favorite_border,
                    color: Colors.red,
                  )
              ],
            ),

            InkWell(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image.asset("images/v11.jpg",
                height: 120,
                width: 120,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.only(bottom: 8),
              alignment: Alignment.center,
              child: Text(
                "Flava eJuice",
                style: TextStyle(fontSize: 10, 
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Details",
                style: TextStyle(
                fontSize: 12,
                color: Color(0xFF4C53A5), 
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\₱149",
                      style: TextStyle(
                fontSize: 11,
                color: Color(0xFF4C53A5), 
                ),
                    ),
                    Icon(
                      Icons.shopping_cart_checkout,
                      color: Color.fromARGB(255, 255, 81, 0), 
                    )
                ]),
            ),
          ],
        ),
        ),

      ],
      );
  }
}