import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'ItemAppBar.dart';
import 'ItemBottomNavBar.dart';

class ItemPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 238, 220),
      body: ListView(
        children: [
          ItemAppBar(),
          Padding(
            padding: EdgeInsets.all(20),
            child: Image.asset("images/1.jpg", height: 300,),
          ),
      const Padding(
                padding: EdgeInsets.all(20),
                child:Text("Flava Disposable", 
                style: TextStyle(
                  fontSize: 28,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                  ),
                  ),  
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RatingBar.builder(
                      initialRating: 4,
                      minRating: 1,
                      direction: Axis.horizontal,
                      itemCount: 5,
                      itemSize: 20,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4),
                      itemBuilder: (context, _) => Icon(
                        Icons.favorite,
                        color: Color.fromARGB(255, 255, 174, 128),
                      ),
                      onRatingUpdate: (index){},
                    ),
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.all(20),
              child: Text(
                "FLAVA AEBAR 5% Nicotine 8500 Puffs 18ML 650mAh Rechargeable Original Ae Bar Type C Disposable Vape Pod.",
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xFF4C53A5),
                ),
              ),
              ),

              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  children: [
                    Text("Flavor:",
                    style: TextStyle(
                      fontSize: 18,
                      color: Color(0xFF4C53A5),
                      fontWeight: FontWeight.bold,
                    ),
                    ), 
                    SizedBox(
                      width: 10,
                    ),
                    Row(children: [
                      for (int i = 1; i < 6; i++)
                      Container(
                        height: 30,
                        width: 30,
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 8,
                            ),
                          ],
                        ),
                        child: Text(
                            i.toString(),
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color:  Color.fromARGB(255, 255, 174, 128),
                              ),
                              ),
                      ),
                    ],),
                  ],
                ) ,
              ),
        ],
      ),
      bottomNavigationBar: ItemBottomNavBar(),
    );
  }
}

