import 'package:flutter/material.dart';

import 'Widgets/CartAppBar.dart';
import 'Widgets/CartBottomNavbar.dart';
import 'Widgets/CartItemSamples.dart';

class CartPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 214, 196), 
      body: ListView(
        children: [
          CartAppBar(),

          Container(
            height: 500,
            padding: EdgeInsets.only(top: 15),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 236, 236, 236),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(35),
                topRight: Radius.circular(35),
              ),
            ),

          child: Column(children: [
            CartItemSamples(),
          ]),
          ),
      ],
      ),
      bottomNavigationBar: CartBottomNavBar(),
    );
  }
}